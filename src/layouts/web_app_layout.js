import React from "react";
import { Menu } from "semantic-ui-react";
import NavBar from "./navigation_bar";

const WebAppLayout = ({ children, myProfile }) => {
  return (
    <div>
      <Menu
        fixed="top"
        visible="true"
        width="wide"
        style={{
          height: "65px",
          position: "fixed",
          zIndex: 1000
        }}
      >
        <NavBar />
      </Menu>
      <div>{children}</div>
    </div>
  );
};

export default WebAppLayout;
