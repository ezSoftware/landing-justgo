/* global window */
import React, { Component } from "react";
import WebAppLayout from "./web_app_layout";
import MobileAppLayout from "./mobile_app_layout";

class AppLayout extends Component {
  render() {
    if (window.innerWidth < 768) {
      return <MobileAppLayout children={this.props.children} />;
    }
    return <WebAppLayout children={this.props.children} />;
  }
}
export default AppLayout;
