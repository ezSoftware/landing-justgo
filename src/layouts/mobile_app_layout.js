/* global window */
import React, { Component } from "react";
import { Label, Icon, Segment, Image, Menu, Sidebar } from "semantic-ui-react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router";

const logo = require("../assets/images/logo.png");

class MobileAppLayout extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false };
  }

  render() {
    const { visible } = this.state;
    return (
      <div
        style={{
          fontFamily: "SFR"
        }}
      >
        <Sidebar.Pushable as={Segment}>
          <Sidebar
            as={Menu}
            animation="overlay"
            visible={visible}
            icon="labeled"
            vertical
            onClick={() => this.setState({ visible: false })}
            style={{ width: "70%" }}
          />
          <Sidebar.Pusher style={{ minHeight: "100vh" }}>
            <Menu
              borderless
              direction="top"
              width="very wide"
              style={{
                height: 0.1 * window.innerHeight,
                backgroundColor: "#fff"
              }}
            >
              <Menu.Item position="left">
                <Link to="/">
                  <Image
                    centered
                    src={logo}
                    size="tiny"
                    style={{ width: 50 }}
                  />
                </Link>
              </Menu.Item>
              <Menu.Item>
                <Label
                  style={{
                    backgroundColor: "transparent",
                    color: "#676767"
                  }}
                >
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    style={{ marginLeft: 10, marginRight: 10 }}
                    href="https://instagram.com/justgoinsta"
                  >
                    <Icon
                      size="big"
                      style={{ backgroundColor: "#fff", color: "#676767" }}
                      name="instagram"
                    />
                  </a>
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    style={{ marginLeft: 10, marginRight: 10 }}
                    href="https://twitter.com/JustGotwit"
                  >
                    <Icon
                      size="big"
                      style={{ backgroundColor: "#fff", color: "#676767" }}
                      name="twitter"
                    />
                  </a>
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    style={{ marginLeft: 10, marginRight: 10 }}
                    href="https://www.facebook.com/JustGoapp/"
                  >
                    <Icon
                      size="big"
                      style={{ backgroundColor: "#fff", color: "#676767" }}
                      name="facebook"
                    />
                  </a>
                </Label>
              </Menu.Item>
            </Menu>
            <Sidebar.Pusher onClick={() => this.setState({ visible: false })}>
              <div style={{ height: "100%" }}>{this.props.children}</div>
            </Sidebar.Pusher>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>
    );
  }
}

export default withRouter(MobileAppLayout);
