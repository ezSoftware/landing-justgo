/* global window */
import React from "react";
import { Icon, Menu, Image } from "semantic-ui-react";

const logo = require("../assets/images/logo.png");

const NavBar = () => {
  return (
    <Menu secondary widths={8} size="tiny">
      <Menu.Item>
        <a href="#0">
          <Image src={logo} style={{ width: 60 }} />
        </a>
      </Menu.Item>
      <Menu.Item style={{ width: 50 }} />
      <Menu.Item>
        <a href="#1">
          <font style={{ fontSize: 13, color: "#676767" }}>
            {window.navigator.userLanguage || window.navigator.language === "es"
              ? "Rutas por ciudades"
              : "City routes"}
          </font>
        </a>
      </Menu.Item>
      <Menu.Item>
        <a href="#2">
          <font style={{ fontSize: 13, color: "#676767" }}>
            {window.navigator.userLanguage || window.navigator.language === "es"
              ? "Conoce viajeros"
              : "Connecting travellers"}
          </font>
        </a>
      </Menu.Item>
      <Menu.Item>
        <a href="#3">
          <font style={{ fontSize: 13, color: "#676767" }}>
            {window.navigator.userLanguage || window.navigator.language === "es"
              ? "Tours interactivos"
              : "Interactive tours"}
          </font>
        </a>
      </Menu.Item>
      <Menu.Item>
        <a href="#4">
          <font style={{ fontSize: 13, color: "#676767" }}>
            {window.navigator.userLanguage || window.navigator.language === "es"
              ? "Únete a viajes"
              : "Join trips"}
          </font>
        </a>
      </Menu.Item>
      <Menu.Item>
        <a href="#5">
          <font style={{ fontSize: 13, color: "#676767" }}>
            {window.navigator.userLanguage || window.navigator.language === "es"
              ? "Planes con locales"
              : "Plans with locals"}
          </font>
        </a>
      </Menu.Item>
      <Menu.Item style={{ width: 80 }} />
      <Menu.Item>
        <a
          rel="noopener noreferrer"
          target="_blank"
          style={{ marginLeft: 10, marginRight: 10 }}
          href="https://instagram.com/justgoinsta"
        >
          <Icon
            size="big"
            style={{ backgroundColor: "#fff", color: "#676767" }}
            name="instagram"
          />
        </a>
        <a
          rel="noopener noreferrer"
          target="_blank"
          style={{ marginLeft: 10, marginRight: 10 }}
          href="https://twitter.com/JustGotwit"
        >
          <Icon
            size="big"
            style={{ backgroundColor: "#fff", color: "#676767" }}
            name="twitter"
          />
        </a>
        <a
          rel="noopener noreferrer"
          target="_blank"
          style={{ marginLeft: 10, marginRight: 10 }}
          href="https://www.facebook.com/JustGoapp/"
        >
          <Icon
            size="big"
            style={{ backgroundColor: "#fff", color: "#676767" }}
            name="facebook"
          />
        </a>
      </Menu.Item>
    </Menu>
  );
};

export default NavBar;
