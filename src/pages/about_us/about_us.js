import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchAboutUs } from "../../actions";
import { Form } from "semantic-ui-react";
import AboutUsView from "./about_us_view";
import { About } from "../../utils/data";
class AboutUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      aboutUs: About
    };
    this.props.fetchAboutUs(this.props.match.params.id);
  }
  /*
  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.about, nextProps.about)) {
      this.setState({ about: nextProps.about });
    }
  }
*/
  render() {
    return (
      <Form onSubmit={event => event.preventDefault()}>
        <AboutUsView aboutUs={this.state.aboutUs} />
      </Form>
    );
  }
}

const mapStateToProps = ({ aboutUs }) => ({
  aboutUs
});

const actions = {
  fetchAboutUs
};

export default connect(mapStateToProps, actions)(AboutUs);
