/* global window */
import React from "react";
import { Icon, Card, Grid, Image } from "semantic-ui-react";

const AboutUsView = ({ aboutUs }) => {
  const windowsWidth = window.innerWidth;
  return (
    <Grid centered style={{ marginTop: 10 }}>
      <Grid.Row>
        <Card
          color="blue"
          style={{
            maxWidth: 400,
            width: windowsWidth * 0.85,
            textAlign: "left"
          }}
        >
          <Image src={aboutUs.photo_url} />
          <Card.Content style={{ fontSize: 17, color: "black" }}>
            <Card.Header style={{ fontSize: 30, marginBottom: 10 }}>
              {aboutUs.name}
            </Card.Header>
            <Card.Meta>
              <span className="date">{aboutUs.job}</span>
            </Card.Meta>
            <Card.Description />
            <a href={`mailto: ${aboutUs.email}`}>
              {" "}
              <p style={{ color: "black" }}>
                <Icon name="mail" style={{ marginTop: 10 }} />
                {aboutUs.email}
              </p>
            </a>
            <br />
            <a href={`tel: ${aboutUs.phone}`}>
              <p style={{ color: "black" }}>
                <Icon name="phone" />
                {aboutUs.phone}
              </p>
            </a>
            <br />
            <a href={`https://api.whatsapp.com/send?phone=${aboutUs.whatsapp}`}>
              <p style={{ color: "black" }}>
                <Icon name="whatsapp" />
                {aboutUs.whatsapp}
              </p>
            </a>
            <br />
            <a href={`${aboutUs.linkedin_url}`}>
              <p style={{ color: "black" }}>
                <Icon name="linkedin" />
                LinkedIn: {aboutUs.name}
              </p>
            </a>
          </Card.Content>
          <Card.Content
            extra
            style={{ backgroundColor: "#dbdbdb", textAlign: "center" }}
          >
            <a href="src/utils/Chris.vcf" download>
              <p style={{ color: "white", fontSize: 20 }}>
                <Icon name="vcard outline" color="grey" />
                Download vCard
              </p>
            </a>
          </Card.Content>
        </Card>
      </Grid.Row>
    </Grid>
  );
};

export default AboutUsView;
