import { combineReducers } from "redux";
import { aboutUsReducer } from "./about_us_reducer.js";

const rootReducer = combineReducers({
  aboutUs: aboutUsReducer
});

export default rootReducer;
