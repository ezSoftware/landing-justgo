import { FETCH_ABOUT_US } from "../actions/action_types";

export function aboutUsReducer(state = {}, action) {
  switch (action.type) {
    case FETCH_ABOUT_US: {
      return {
        data: action.payload.data.about,
        error: {}
      };
    }
    default: {
      return state;
    }
  }
}
