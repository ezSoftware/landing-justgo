import { axiosInstance } from "./configured_axios";
import { FETCH_ABOUT_US } from "./action_types";

export function fetchAboutUs(name) {
  return dispatch =>
    axiosInstance.get(`/about_us/${name}`).then(response => {
      dispatch({ type: FETCH_ABOUT_US, payload: response });
    });
}
