const ROOT_DOMAIN = "routesapi.herokuapp.com";
const ROOT_URL = `https://${ROOT_DOMAIN}`;

export default ROOT_URL;
