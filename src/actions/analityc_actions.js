import { axiosInstance } from "./configured_axios";
import { SEND_ANALITYC } from "./action_types";

export function analitycs(group, subgroup, name) {
  const request = axiosInstance.post(`/analitycs`, {
    analityc: { group, subgroup, name }
  });

  return {
    type: SEND_ANALITYC,
    payload: request
  };
}
