export const LandingData = [
  {
    odd: true,
    id: "1",
    message:
      "With Just Go you can find the best tourist routes that other travelers have made and create your own from scratch or using the route of another traveler",
    popup_message: [
      "Choose a city, the number of days you will spend in it, your personal interests and the points of the city that you do not want to miss. \n Select a tourist route and you will have your trip planned for days, with a map where you will see the points to visit every day and all the information of each of them."
    ],
    message_sp: [
      ,
      "Con Just Go podrás encontrar las mejores rutas turísticas que han hecho otros viajeros y crear las tuyas propias desde cero o usando la de otro viajero."
    ],
    popup_message_sp: [
      "Elige una ciudad, el número de días que vas a pasar en ella, tus intereses personales y los puntos de la ciudad que no quieres perderte.",
      "Selecciona una ruta turística y tendrás tu viaje planeado por días, con un mapa donde verás los puntos a visitar cada dia y toda la información de cada uno de ellos."
    ],
    button: "Discover the best tours",
    title:
      "Are you going to a city and would you like to know what to visit in it?",
    title_sp: "¿Vas de viaje a una ciudad y te gustaría saber qué visitar?"
  }
];

export const About = {
  name: "Chris",
  email: "chrcasarc@gmail.com",
  job: "Co Founder and Software Engineer",
  phone: "+352 85 039 8529",
  whatsapp: "34675221928",
  linkedin_url: "https://www.linkedin.com/in/christiancastillejo",
  photo_url:
    "https://scontent-dub4-1.xx.fbcdn.net/v/t1.0-9/1545001_10207710904625161_5727350927661114185_n.jpg?_nc_cat=0&oh=58c0302d4adffe9bdfc52ae65d58ecd6&oe=5B7FCFD6"
};

export const User = {
  user: {
    id: 1,
    email: "jorgeregidor@gmail.com",
    first_name: "Jorge",
    last_name: "Regidor",
    date_of_birth: "1986-04-02",
    bio: "I am a web developer",
    rate: 5,
    gender: "M",
    routes: [
      {
        id: 1,
        title: "Route 0",
        slogan: "Slogan 0",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        rate: 4.5,
        address: "Dublin",
        ndays: "3",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 1,
            title: "Cultural"
          },
          {
            id: 2,
            title: "Family"
          },
          {
            id: 3,
            title: "Tourism"
          }
        ]
      },
      {
        id: 2,
        title: "Route 1",
        slogan: "Slogan 1",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        rate: 4.5,
        address: "Dublin",
        ndays: "3",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 4,
            title: "Cultural"
          },
          {
            id: 5,
            title: "Family"
          },
          {
            id: 6,
            title: "Tourism"
          }
        ]
      },
      {
        id: 11,
        title: "First",
        slogan: "Save test",
        description: "This is a route for test",
        rate: 0,
        address: "drumcomdra",
        ndays: "0",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 31,
            title: "interest1"
          },
          {
            id: 32,
            title: "interest2"
          }
        ]
      },
      {
        id: 12,
        title: "asd",
        slogan: "asdasdsad",
        description: "asda asdsas",
        rate: 0,
        address: "Denver, CO, USA",
        ndays: "0",
        status: "published",
        favourite: "false",
        interests: []
      },
      {
        id: 3,
        title: "Route 2",
        slogan: "Slogan 2",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        rate: 4.5,
        address: "Dublin",
        ndays: "3",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 7,
            title: "Cultural"
          },
          {
            id: 8,
            title: "Family"
          },
          {
            id: 9,
            title: "Tourism"
          }
        ]
      },
      {
        id: 4,
        title: "Route 3",
        slogan: "Slogan 3",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        rate: 4.5,
        address: "Dublin",
        ndays: "3",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 10,
            title: "Cultural"
          },
          {
            id: 11,
            title: "Family"
          },
          {
            id: 12,
            title: "Tourism"
          }
        ]
      },
      {
        id: 5,
        title: "Route 4",
        slogan: "Slogan 4",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        rate: 4.5,
        address: "Dublin",
        ndays: "3",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 13,
            title: "Cultural"
          },
          {
            id: 14,
            title: "Family"
          },
          {
            id: 15,
            title: "Tourism"
          }
        ]
      },
      {
        id: 6,
        title: "Route 5",
        slogan: "Slogan 5",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        rate: 4.5,
        address: "Dublin",
        ndays: "3",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 16,
            title: "Cultural"
          },
          {
            id: 17,
            title: "Family"
          },
          {
            id: 18,
            title: "Tourism"
          }
        ]
      },
      {
        id: 7,
        title: "Route 6",
        slogan: "Slogan 6",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        rate: 4.5,
        address: "Dublin",
        ndays: "3",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 19,
            title: "Cultural"
          },
          {
            id: 20,
            title: "Family"
          },
          {
            id: 21,
            title: "Tourism"
          }
        ]
      },
      {
        id: 8,
        title: "Route 7",
        slogan: "Slogan 7",
        description:
          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        rate: 4.5,
        address: "Dublin",
        ndays: "3",
        status: "published",
        favourite: "false",
        interests: [
          {
            id: 22,
            title: "Cultural"
          },
          {
            id: 23,
            title: "Family"
          },
          {
            id: 24,
            title: "Tourism"
          }
        ]
      }
    ]
  }
};

export const Route = {
  id: 1,
  title: "Route 1",
  slogan: "Slogan 1",
  description:
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  rate: 4.5,
  address: "Dublin",
  ndays: "3",
  interests: [
    {
      title: "Cultural"
    },
    {
      title: "Family"
    },
    {
      title: "Tourism"
    }
  ],
  days: [
    {
      id: 1,
      number: 1,
      points: [
        {
          id: 1,
          title: "Spire",
          time: 0.5,
          schedule_start: "2000-01-01T00:00:00.000Z",
          schedule_end: "2000-01-01T23:59:59.000Z",
          price_min: 0,
          price_max: 0,
          currency: "EUR",
          category: "Monument",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
          gplace: {
            id: 1,
            place_id: "ChIJQb1mPoQOZ0gR95oP3xuBeEk",
            location_lat: 53.3498091,
            location_lon: -6.2602548,
            address: "North City, Dublin, Ireland"
          }
        },
        {
          id: 2,
          title: "O'Connell Bridge",
          time: 1,
          schedule_start: "2000-01-01T00:00:00.000Z",
          schedule_end: "2000-01-01T23:59:59.000Z",
          price_min: 0,
          price_max: 0,
          currency: "EUR",
          category: "Monument",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
          gplace: {
            id: 2,
            place_id: "ChIJ0yvD5YQOZ0gR7e7Be-3rC3E",
            location_lat: 53.3472521,
            location_lon: -6.261468,
            address: "O'Connell Bridge, Dublin, Ireland"
          }
        },
        {
          id: 3,
          title: "Temple Bar",
          time: 3,
          schedule_start: "2000-01-01T08:00:00.000Z",
          schedule_end: "2000-01-01T04:00:00.000Z",
          price_min: 0,
          price_max: 0,
          currency: "EUR",
          category: "Pub",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
          gplace: {
            id: 3,
            place_id: "ChIJj8AjLIMOZ0gRg9oCVMgV0yc",
            location_lat: 53.3454357,
            location_lon: -6.266874,
            address: "1-2 Temple Bar, Dublin 2, D02 RT29, Ireland"
          }
        }
      ],
      route_directions: [
        {
          id: 1,
          origin: {
            lat: 53.3498091,
            lng: -6.2602548
          },
          destination: {
            lat: 53.3472521,
            lng: -6.261468
          }
        },
        {
          id: 2,
          origin: {
            lat: 53.3472521,
            lng: -6.261468
          },
          destination: {
            lat: 53.3454357,
            lng: -6.266874
          }
        }
      ]
    },
    {
      id: 2,
      number: 2,
      points: [
        {
          id: 1,
          title: "St Stephen's Green",
          time: 2.5,
          schedule_start: "2000-01-01T10:00:00.000Z",
          schedule_end: "2000-01-01T21:00:00.000Z",
          price_min: 0,
          price_max: 0,
          currency: "EUR",
          category: "Park",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
          gplace: {
            id: 4,
            place_id: "ChIJo5EyLpkOZ0gRfIOtBfARzQU",
            location_lat: 53.3381768,
            location_lon: -6.261313,
            address: "St Stephen's Green, Dublin 2, Ireland"
          }
        },
        {
          id: 2,
          title: "St Patrick's",
          time: 1,
          schedule_start: "2000-01-01T10:00:00.000Z",
          schedule_end: "2000-01-01T18:00:00.000Z",
          price_min: 7,
          price_max: 12,
          currency: "EUR",
          category: "Monument",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
          gplace: {
            id: 5,
            place_id: "ChIJpVg1bAmnREgRVLEltpvmyJI",
            location_lat: 53.3395186,
            location_lon: -6.2736707,
            address:
              "St Patrick's Close, Wood Quay, Dublin 8, Co. Dublin, DZ08 H6X3, Ireland"
          }
        },
        {
          id: 3,
          title: "Dicey's",
          time: 3,
          schedule_start: "2000-01-01T12:00:00.000Z",
          schedule_end: "2000-01-01T02:00:00.000Z",
          price_min: 0,
          price_max: 10,
          currency: "EUR",
          category: "Pub",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
          gplace: {
            id: 6,
            place_id: "ChIJu95ZvZ8OZ0gRPY7NgZec_0g",
            location_lat: 53.3358851,
            location_lon: -6.263717,
            address: "21-25 Harcourt St, Saint Kevin's, Dublin 2, Ireland"
          }
        }
      ],
      route_directions: [
        {
          id: 1,
          origin: {
            lat: 53.3381768,
            lng: -6.261313
          },
          destination: {
            lat: 53.3395186,
            lng: -6.2736707
          }
        },
        {
          id: 2,
          origin: {
            lat: 53.3395186,
            lng: -6.2736707
          },
          destination: {
            lat: 53.3357945,
            lng: -6.265782
          }
        }
      ]
    },
    {
      id: 3,
      number: 3,
      points: [
        {
          id: 1,
          title: "Guinnes Storehouse",
          time: 2,
          schedule_start: "2000-01-01T10:00:00.000Z",
          schedule_end: "2000-01-01T21:00:00.000Z",
          price_min: 14,
          price_max: 20,
          currency: "EUR",
          category: "Museum",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
          gplace: {
            id: 7,
            place_id: "ChIJawXFQIQOZ0gRoucFdZIzGbM",
            location_lat: 53.3427957,
            location_lon: -6.2898632,
            address: "St James's Gate, Ushers, Dublin 8, Co. Dublin, Ireland"
          }
        },
        {
          id: 2,
          title: "Phoneix Park",
          time: 4,
          schedule_start: "2000-01-01T10:00:00.000Z",
          schedule_end: "2000-01-01T21:00:00.000Z",
          price_min: 0,
          price_max: 0,
          currency: "EUR",
          category: "Park",
          description:
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt",
          gplace: {
            id: 8,
            place_id: "ChIJdfSYpAANZ0gRmPPa6LzNsOI",
            location_lat: 53.3558855,
            location_lon: -6.3320073,
            address: "Phoenix Park, Dublin, Ireland"
          }
        }
      ]
    }
  ],
  user: {
    id: 1,
    email: "jorgeregidor@gmail.com",
    first_name: "Jorge",
    last_name: "Regidor",
    date_of_birth: "1986-04-02",
    bio: "I am a web developer",
    rate: 5,
    gender: "M"
  }
};
