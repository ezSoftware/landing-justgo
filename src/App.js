import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";

/* Layouts */
import AppLayout from "./layouts/app_layout";

/* Landing page */
import LandingPage from "./landing/landing_page.js";

import store from "./store";

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <AppLayout>
          <Route exact path="/" component={LandingPage} />
        </AppLayout>
      </Router>
    </Provider>
  );
};

export default App;
