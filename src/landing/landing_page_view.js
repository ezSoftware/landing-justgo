/*global window*/
import React from "react";
import { Label, Modal, Grid, Icon, Button } from "semantic-ui-react";
import "../assets/fonts/extrafonts.css";
import ScrollableAnchor from "react-scrollable-anchor";
import HeroImage from "../assets/images/backGo.jpg";
import HeroImageMobile from "../assets/images/backGoMobile.jpg";
import HeroImageMobile2 from "../assets/images/backGoMobile2.jpg";
const windowHeight = window.innerHeight;
const windowWidth = window.innerWidth;
const LandingPageView = ({
  data,
  openModal,
  isModalOpen,
  closeModal,
  analitycs,
  button,
  setModal,
  modal,
  modalButton,
  language
}) => {
  return (
    <div
      style={{
        backgroundImage: windowWidth > 700 && `url(${HeroImage})`,
        backgroundAttachment: windowWidth > 700 && "fixed",
        backgroundSize: windowWidth > 700 && "cover"
      }}
    >
      <ScrollableAnchor id={"0"}>
        <div
          style={{
            backgroundColor: windowWidth > 700 && "transparent",
            backgroundImage: windowWidth < 700 && `url(${HeroImageMobile})`,
            backgroundSize: windowWidth < 700 && "cover"
          }}
        >
          <Grid
            stackable
            centered
            style={{
              height: windowWidth < 700 ? windowHeight : 1.05 * windowHeight
            }}
          >
            <Grid.Row verticalAlign="middle">
              <Grid.Column style={{ textAlign: "center" }}>
                <h1
                  style={{
                    marginTop:
                      windowWidth < 700
                        ? windowHeight * 0.05
                        : windowHeight * 0.35,
                    color: "#fff",
                    fontFamily: "SFDB",
                    fontSize: windowWidth < 700 ? 35 : 55
                  }}
                >
                  JUST GO
                </h1>
                <p
                  style={{
                    lineHeight: 1.2,
                    color: "#fff",
                    paddingLeft:
                      windowWidth < 700 ? windowWidth * 0.0 : windowWidth * 0.2,
                    paddingRight:
                      windowWidth < 700 ? windowWidth * 0.0 : windowWidth * 0.2,
                    fontSize: windowWidth < 700 ? 22 : 22,
                    fontWeight: 70,
                    fontFamily: "SFDB"
                  }}
                >
                  {language === "es"
                    ? [
                        [
                          "La nueva app de viajes que te permite descubrir",
                          <br />,
                          "lugares y viajeros de manera fácil e intuitiva."
                        ],
                        <br />
                      ]
                    : [
                        "The new travel app that allows you discover places ",
                        "and travelers in an easy and intuitive way."
                      ]}
                </p>
                <p
                  style={{
                    marginTop:
                      windowWidth < 700
                        ? windowHeight * 0.1
                        : windowHeight * 0.05,
                    lineHeight: 1.2,
                    color: "#fff",
                    paddingLeft:
                      windowWidth < 700 ? windowWidth * 0.0 : windowWidth * 0.2,
                    paddingRight:
                      windowWidth < 700 ? windowWidth * 0.0 : windowWidth * 0.2,
                    fontSize: windowWidth < 700 ? 20 : 20,
                    fontWeight: 70,
                    fontFamily: "SFDB"
                  }}
                >
                  {language === "es"
                    ? "JUST GO APP SERÁ LANZADA MUY PRONTO!"
                    : "JUST GO APP WILL BE LAUNCHING SOON!"}
                </p>
                <p
                  style={{
                    marginTop: -(windowHeight * 0.02),
                    lineHeight: 1.2,
                    color: "#fff",
                    paddingLeft:
                      windowWidth < 700 ? windowWidth * 0.0 : windowWidth * 0.2,
                    paddingRight:
                      windowWidth < 700 ? windowWidth * 0.0 : windowWidth * 0.2,
                    fontSize: windowWidth < 700 ? 18 : 18,
                    fontWeight: 70,
                    fontFamily: "SFDR"
                  }}
                >
                  {language === "es"
                    ? [
                        "Hasta entonces, puedes echar un vistazo a",
                        <br />,
                        "nuestros futuros servicios."
                      ]
                    : [
                        "Until then, you can check out our",
                        <br />,
                        " up and coming services below."
                      ]}
                </p>
                <a href="#1">
                  <Icon
                    size="big"
                    name="arrow alternate circle down outline"
                    style={{
                      marginTop: windowHeight * 0.1,
                      color: "#fff"
                    }}
                  />
                </a>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row />
          </Grid>
        </div>
      </ScrollableAnchor>
      {data &&
        data.map(data => (
          <ScrollableAnchor
            id={
              (data.id === "1" && windowWidth < 700) || windowWidth > 700
                ? data.id
                : ""
            }
          >
            <div
              style={{
                backgroundColor: data.odd ? "#fff" : "transparent",
                backgroundImage:
                  windowWidth < 700 && !data.odd && `url(${HeroImageMobile2})`,
                backgroundSize: windowWidth < 700 && !data.odd && "cover"
              }}
            >
              <Grid
                stackable
                centered
                style={{
                  height:
                    windowWidth < 700 ? windowHeight * 0.7 : windowHeight * 0.6
                }}
              >
                <Grid.Row verticalAlign="middle">
                  <Grid.Column style={{ textAlign: "center" }}>
                    <h1
                      style={{
                        color: data.odd ? "#676767" : "#fff",
                        fontFamily: "SFDB",
                        fontSize: windowWidth < 700 ? 20 : 20
                      }}
                    >
                      {language === "es" ? data.title_sp : data.title}
                    </h1>
                    <p
                      style={{
                        lineHeight: 1.2,
                        color: data.odd ? "#676767" : "#fff",
                        paddingLeft:
                          windowWidth < 700
                            ? windowWidth * 0.0
                            : windowWidth * 0.2,
                        paddingRight:
                          windowWidth < 700
                            ? windowWidth * 0.0
                            : windowWidth * 0.2,
                        fontSize: windowWidth < 700 ? 20 : 20,
                        fontWeight: 70,
                        fontFamily: "SFDR"
                      }}
                    >
                      {language === "es" ? data.message_sp : data.message}
                    </p>
                    <Button
                      style={{
                        color: "#fff",
                        marginLeft:
                          windowWidth < 700
                            ? windowWidth * 0.1
                            : windowWidth * 0.15,
                        marginRight:
                          windowWidth < 700
                            ? windowWidth * 0.1
                            : windowWidth * 0.15,
                        backgroundColor: "#00b8ad",
                        borderRadius: "25px",
                        fontSize: windowWidth < 700 ? 15 : 15,
                        fontFamily: "SFDB"
                      }}
                      onClick={() => {
                        setModal(
                          language === "es"
                            ? data.popup_message_sp
                            : data.popup_message,
                          data.button
                        );
                        openModal();
                        analitycs(data.name);
                      }}
                    >
                      {language === "es" ? "Saber más" : "Learn more"}
                    </Button>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </div>
          </ScrollableAnchor>
        ))}
      <Modal
        style={{
          marginTop:
            windowWidth < 700 ? windowHeight * 0.15 : windowHeight * 0.18,
          marginLeft:
            windowWidth < 700 ? windowWidth * 0.02 : windowWidth * 0.3,
          width: windowWidth < 700 ? windowWidth * 0.9 : windowWidth * 0.4,
          borderRadius: 20
        }}
        open={isModalOpen}
        dimmer="blurring"
        onClose={() => closeModal()}
      >
        <Modal.Content
          style={{
            borderRadius: 20
          }}
        >
          <Grid stackable centered>
            <Grid.Row verticalAlign="middle">
              <Grid.Column style={{ textAlign: "center" }}>
                <p
                  style={{
                    marginTop: 20,
                    color: "#676767",
                    fontSize: windowWidth < 700 ? 18 : 17
                  }}
                >
                  {modal}
                </p>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row verticalAlign="middle">
              <Grid.Column style={{ textAlign: "center" }}>
                <Button
                  style={{
                    color: "#fff",
                    backgroundColor: "#00b8ad",
                    borderRadius: "25px",
                    fontSize: windowWidth < 700 ? 15 : 12,
                    fontFamily: "SFDB"
                  }}
                  onClick={() => {
                    closeModal();
                  }}
                >
                  {language === "es" ? "¡Suena bien!" : "Sounds good!"}
                </Button>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row style={{ marginBottom: 10, justifyContent: "center" }}>
              <Grid.Column style={{ textAlign: "center" }}>
                <Label
                  style={{
                    backgroundColor: "transparent",
                    color: "#FFFFFF"
                  }}
                >
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    style={{ marginLeft: 10, marginRight: 10 }}
                    href="https://instagram.com/justgoinsta"
                  >
                    <Icon
                      size="big"
                      style={{ backgroundColor: "#fff", color: "#676767" }}
                      name="instagram"
                    />
                  </a>
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    style={{ marginLeft: 10, marginRight: 10 }}
                    href="https://twitter.com/JustGotwit"
                  >
                    <Icon
                      size="big"
                      style={{ backgroundColor: "#fff", color: "#676767" }}
                      name="twitter"
                    />
                  </a>
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    style={{ marginLeft: 10, marginRight: 10 }}
                    href="https://www.facebook.com/JustGoapp/"
                  >
                    <Icon
                      size="big"
                      style={{ backgroundColor: "#fff", color: "#676767" }}
                      name="facebook"
                    />
                  </a>
                </Label>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </Modal.Content>
      </Modal>
      {windowWidth < 600 && (
        <Label
          style={{
            marginLeft: 20,
            marginBottom: 30,
            backgroundColor: "transparent",
            color: "#676767"
          }}
        >
          <a
            rel="noopener noreferrer"
            target="_blank"
            style={{ marginLeft: 10, marginRight: 10 }}
            href="https://instagram.com/justgoinsta"
          >
            <Icon
              size="big"
              style={{ backgroundColor: "#fff", color: "#676767" }}
              name="instagram"
            />
          </a>
          <a
            rel="noopener noreferrer"
            target="_blank"
            style={{ marginLeft: 10, marginRight: 10 }}
            href="https://twitter.com/JustGotwit"
          >
            <Icon
              size="big"
              style={{ backgroundColor: "#fff", color: "#676767" }}
              name="twitter"
            />
          </a>
          <a
            rel="noopener noreferrer"
            target="_blank"
            style={{ marginLeft: 10, marginRight: 10 }}
            href="https://www.facebook.com/JustGoapp/"
          >
            <Icon
              size="big"
              style={{ backgroundColor: "#fff", color: "#676767" }}
              name="facebook"
            />
          </a>
        </Label>
      )}
    </div>
  );
};

export default LandingPageView;
