import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { analitycs } from "../actions/index";
import LandingPageView from "./landing_page_view";

class RouteList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [
        {
          odd: true,
          id: "1",
          message:
            "With Just Go, you can find the best tourist routes that other travelers have made and create your own from scratch. Or you can use the route created by another traveler.",
          popup_message: [
            "Choose a city, the number of days you will spend there, your personal interests and the places in the city that you do not want to miss.",
            <br />,
            "Create your own itinerary or select an itinerary built by a previous tourist and you will have your trip planned for days. "
          ],
          message_sp: [
            "Con Just Go podrás encontrar las mejores rutas turísticas que han hecho otros viajeros y crear las tuyas propias desde cero o usando la de otro viajero."
          ],
          popup_message_sp: [
            "Elige una ciudad, el número de días que vas a pasar en ella, tus intereses personales y los puntos de la ciudad que no quieres perderte.",
            <br />,
            "Selecciona una ruta turística y tendrás tu viaje planeado por días, con un mapa donde verás los puntos a visitar cada dia y toda la información de cada uno de ellos."
          ],
          button: "Discover the best tours",
          title:
            "Are you going to a city and would like to know what you should visit?",
          title_sp:
            "¿Vas de viaje a una ciudad y te gustaría saber qué visitar?",
          name: "routes"
        },
        {
          id: "2",
          message_sp: [
            "Con JustGo app podrás hacer ambas a la vez. Conoce y habla con viajeros que están viajando al mismo destino en la misma fecha que tú."
          ],
          popup_message_sp: [
            "Solo selecciona la ciudad a la que vas a viajar, filtra por intereses y desliza a izquierda o derecha para empezar una conversación. Las mejores historias empiezan con Just Go!"
          ],
          message: [
            "With JustGo, you can do both at the same time. Connect and talk to travelers who are traveling to the same destination on the same date as you."
          ],
          popup_message:
            "Just select the city you are going to travel to, filter by interests, and swipe left or right to start a conversation. The best stories start with Just Go!",
          button: "Turn the phone into your interactive guide",
          title_sp: "¿Te gusta viajar y conocer gente nueva?",
          title: "Do you like to travel and meet new people?",
          name: "connect"
        },
        {
          odd: true,
          id: "3",
          message: [
            "Just Go makes it easy to do this, you just need to join trips that other travelers have organised."
          ],
          popup_message: [
            "You only have to choose the places that you like the most.",
            <br />,
            "And if you travel with friends and you want to share your trip... the more the merrier",
            <br />,
            "Publish your trip and invite other travelers to join you!"
          ],
          message_sp: [
            "Just Go hace que eso ya no sea un problema. Solo tienes que unirte a viajes que han organizado otros viajeros."
          ],
          popup_message_sp: [
            "Solo tendrás que escoger el lugar que más te guste.",
            <br />,
            "Y si viajas con amigos y quieres compartir tu viaje...",
            <br />,
            "¡Donde caben 2 caben 3!",
            " Publica tu viaje para que otros viajeros se unan."
          ],
          button: "Meet travellers",
          title_sp:
            "¿Te gustaría viajar a una ciudad pero no tienes con quien hacerlo?",
          title:
            "Would you like to travel to a city but don't  have anyone to go with?",
          name: "trips"
        },

        {
          id: "4",
          message: [
            "Free tours have been around for years, it's time for free tours 2.0.",
            <br />,
            "Just Go will be your personal tour guide with interactive maps, audio and videos of the places of interest in the city."
          ],
          popup_message: [
            "You only need your mobile and your headphones to learn about each points of interest of the city.",
            <br />,
            "Professional tour guides create the content, teaching you about the best parts of the city."
          ],
          message_sp: [
            "Desde hace años existen los free tours. Es hora que lleguen los free tours 2.0.",
            <br />,
            "Just Go será tu guía turístico personal con mapas interactivos, audios y videos de los lugares de interés de la ciudad."
          ],
          popup_message_sp: [
            "Tu solo necesitarás tu móvil y tus auriculares para conocer cada punto de la ciudad de manera interactiva.",
            <br />,
            "Guías turísticos profesionales se han encargado de crear contenidos para contarte lo mejor de la ciudad."
          ],
          button: "Discover more",
          title: "Would you like to have a real tour guide on your phone?",
          title_sp:
            "¿Te gustaría tener un verdadero guía turístico en tu movil?",
          name: "tours"
        },

        {
          odd: true,
          id: "5",
          message: [
            "Sometimes it's funner to meet local people and join their plans and live the city as a local!"
          ],
          popup_message:
            "Do you want to meet interesting people without leaving your city? Share your plans with travelers from all over the world, and discover their experiences.",
          message_sp: [
            "A veces es mejor conocer a personas locales y unirte a sus planes para vivir la ciudad como realmente la viven ellos."
          ],
          popup_message_sp: [
            "¿Quieres conocer a personas interesantes sin salir de tu ciudad? Comparte tus planes con viajeros de todo el mundo, y descubre sus experiencias."
          ],

          button: "Connect local people for the best experience",
          title_sp: "¿No te gustan los típicos planes de turistas?",
          title: "Frustrated with typical tourist plans?",
          name: "locals"
        }
      ],
      isModalOpen: false,
      modal: "",
      modalButton: "",
      language: window.navigator.language
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.analitycs = this.analitycs.bind(this);
    this.setModal = this.setModal.bind(this);
  }

  openModal() {
    this.setState({ isModalOpen: true });
  }

  closeModal() {
    this.setState({ isModalOpen: false });
  }

  setModal(message, modalButton) {
    this.setState({ modal: message, modalButton: modalButton });
  }

  analitycs(name) {
    this.props.analitycs("landing", "newFeature", name);
  }

  render() {
    return (
      <LandingPageView
        data={this.state.data}
        modal={this.state.modal}
        modalButton={this.state.modalButton}
        setModal={this.setModal}
        openModal={this.openModal}
        isModalOpen={this.state.isModalOpen}
        closeModal={this.closeModal}
        analitycs={this.analitycs}
        language={this.state.language}
      />
    );
  }
}

export default withRouter(connect("", { analitycs })(RouteList));
